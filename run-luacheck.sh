#!/bin/bash
set -e

DIFFLUAFILES="$(git diff origin/$BITBUCKET_PR_DESTINATION_BRANCH..origin/$BITBUCKET_BRANCH \
    --name-only \
    --diff-filter=ACMR \
    | grep -E "lua$" )"

if [[ ! -z "$DIFFLUAFILES" ]]; then
    echo "---------------------------------"
    echo "Checking Files"
    echo "---------------------------------"
    echo ""
    luacheck $DIFFLUAFILES
else
    echo "No files to check!"
fi

echo ""
echo "---------------------------------"
echo "Finished luacheck"
echo "---------------------------------"
echo ""


