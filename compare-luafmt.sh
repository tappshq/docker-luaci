#!/bin/bash

output_file=$(mktemp)

(
    while read f; do
        (if ! luafmt $f | cmp --silent $f; then
            echo $f >> $output_file
        fi) &
    done < /dev/stdin
    wait
    # This wait must be also inside the pipe, otherwise it doesn't work
    # https://arstechnica.com/civis/viewtopic.php?t=1221315
)

if [ -s $output_file ]; then
    echo "Formatting differences were found in:"
    cat $output_file
    echo "Did you run luafmt before commiting?"
    exit 1
fi
exit 0
